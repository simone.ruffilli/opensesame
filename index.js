var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

var fs = require('fs');

function getUsers() {
    var ret = fs.readFileSync(__dirname + "/" + "users.json", 'utf8');
    console.log('users: ' + ret);
    return JSON.parse(ret) || {
        "admins": [],
        "users": []
    };
}

function isAdmin(userId) {
    return getUsers().admins.indexOf(userId) > -1;
}

function isUser(userId) {
    return getUsers().users.indexOf(userId) > -1;
}

function addUser(req, userType) { //userType {"users","admins"}
    userList = getUsers();
    userToAdd = req.params.userId || null;
    authorizedUser = req.body.adminUserId || null;
    if (userToAdd) {
        if (authorizedUser && isAdmin(authorizedUser)) {
            userList[userType].push(userToAdd);
            userList[userType] = Array.from(new Set(userList[userType])); //Avoid double adding a user
            var write = fs.writeFileSync(__dirname + "/" + "users.json", JSON.stringify(userList), 'utf8')
            return { status: 200, message: {"success": "Success"} };
        } else {
          return { status: 403, message: {"error": "Unauthorized"} };
        }
    } else {
      return { status: 400, message: {"error": "Bad data"} };
    }
}

function deleteUser(req, userType) {
    userList = getUsers();
    userToDelete = req.params.userId || null;
    authorizedUser = req.body.adminUserId || null;
    if (userToDelete) {
        if (authorizedUser && isAdmin(authorizedUser)) {
            userList[userType] = userList[userType].filter( x => x != userToDelete);
            var write = fs.writeFileSync(__dirname + "/" + "users.json", JSON.stringify(userList), 'utf8')
            return { status: 200, message: {"success": "Success"} };
        } else {
          return { status: 403, message: {"error": "Unauthorized"} };
        }
    } else {
      return { status: 400, message: {"error": "Bad data"} };
    }
}

var router = express.Router();

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

app.use('/api', router);

router.route('/users')
  .get(function(req, res) {
    console.log('req: ' + JSON.stringify(req.body));

    userList = getUsers();
    res.status(200).send(JSON.stringify(userList.users));
  });
  
router.route('/user/:userId')
  .post(function(req, res) {
    console.log('req: ' + JSON.stringify(req.body));
    ret = addUser(req, 'users');
    console.log(ret);
    res.status(ret.status).send(ret.message);
  })
  .delete(function(req, res) {
    console.log('req: ' + JSON.stringify(req.body));
    ret = deleteUser(req, 'users');
    console.log(ret);
    res.status(ret.status).send(ret.message);
  });
  
  
router.route('/admin/:userId')
  .post(function(req, res) {
    console.log('req: ' + JSON.stringify(req.body));
    ret = addUser(req, 'admins');
    res.status(ret.status).send(ret.message);
  })
  .delete(function(req, res) {
    console.log('req: ' + JSON.stringify(req.body));
    ret = deleteUser(req, 'admins');
    console.log(ret);
    res.status(ret.status).send(ret.message);
  });


router.route('/lock/open')
  .put(function(req, res) {
    console.log('req: ' + JSON.stringify(req.body));

    var requestingUser = req.body.userId;

    if (isUser(requestingUser)) {
        //Open the lock ;)
        res.status(200).send('{"success": "Success"}');
    } else {
        res.status(403).send('{"error": "Unauthorized"}');
    }
  });


var server = app.listen(8081, function() {
    var host = server.address().address
    var port = server.address().port
    console.log("Listening at http://%s:%s", host, port)
});
